set(boot_source_files
    boot.asm)
add_library(boot ${boot_source_files})

target_link_libraries(boot kernel)
